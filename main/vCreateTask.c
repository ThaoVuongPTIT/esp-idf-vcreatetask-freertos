/* Blink Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "sdkconfig.h"

#include "output.h"
#define BLINK_GPIO 2
static void vTask1(void)
{
    while(1)
    {
        printf("Hello Word!!\n");
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}

static void vTask2(void)
{
    while(1)
    {
        output_toggle(2);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}
static void vTask3(void)
{
    while(1)
    {
        output_toggle(4);
        printf("Hello Vuong Viet Thao\n");
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}
void app_main(void)
{
    output_create(2);
    output_create(4);
    xTaskCreate(vTask1, "Task1", 256, NULL, 4, NULL);
    xTaskCreate(vTask2, "Task2", 256, NULL, 5, NULL);
    xTaskCreate(vTask3, "Task3", 256, NULL, 2, NULL);
    /*độ ưu tiên càng cao thì được thực hiện trước nè*/
}

